# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# SPDX-FileCopyrightText: 2015, 2018, 2019, 2020, 2021, 2022, 2024 Vincenzo Reale <smart2128vr@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: kcm_pulseaudio\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-24 00:39+0000\n"
"PO-Revision-Date: 2024-03-18 10:12+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.02.0\n"

#: kcm/ui/CardListItem.qml:52 kcm/ui/DeviceListItem.qml:135
#, kde-format
msgctxt "@label"
msgid "Profile:"
msgstr "Profilo:"

#: kcm/ui/DeviceListItem.qml:48
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: kcm/ui/DeviceListItem.qml:89
#, kde-format
msgid "Port:"
msgstr "Porta:"

#: kcm/ui/DeviceListItem.qml:112 qml/listitemmenu.cpp:364
#, kde-format
msgctxt "Port is unavailable"
msgid "%1 (unavailable)"
msgstr "%1 (non disponibile)"

#: kcm/ui/DeviceListItem.qml:114 qml/listitemmenu.cpp:366
#, kde-format
msgctxt "Port is unplugged"
msgid "%1 (unplugged)"
msgstr "%1 (non collegata)"

#: kcm/ui/DeviceListItem.qml:189
#, kde-format
msgctxt "Placeholder is channel name"
msgid "%1:"
msgstr "%1:"

#: kcm/ui/DeviceListItem.qml:214
#, kde-format
msgctxt "Perform an audio test of the device"
msgid "Test"
msgstr "Prova"

#: kcm/ui/DeviceListItem.qml:223
#, kde-format
msgctxt "Audio balance (e.g. control left/right volume individually"
msgid "Balance"
msgstr "Bilanciamento"

#: kcm/ui/main.qml:80
#, kde-format
msgid "Show Inactive Devices"
msgstr "Mostra i dispositivi inattivi"

#: kcm/ui/main.qml:87
#, kde-format
msgid "Configure Volume Controls…"
msgstr "Configura i controlli del volume..."

#: kcm/ui/main.qml:94
#, kde-format
msgid "Configure…"
msgstr "Configura…"

#: kcm/ui/main.qml:97
#, kde-format
msgid "Requires %1 PulseAudio module"
msgstr "Richiede il modulo PulseAudio %1"

#: kcm/ui/main.qml:100
#, kde-format
msgid ""
"Add virtual output device for simultaneous output on all local sound cards"
msgstr ""
"Aggiungi il dispositivo virtuale per l'uscita simultanea su tutte le schede "
"audio locali"

#: kcm/ui/main.qml:106
#, kde-format
msgid ""
"Automatically switch all running streams when a new output becomes available"
msgstr ""
"Commuta automaticamente tutti i flussi in riproduzione quando è disponibile "
"una nuova uscita"

#: kcm/ui/main.qml:143
#, kde-format
msgid "Playback Devices"
msgstr "Dispositivi di riproduzione"

#: kcm/ui/main.qml:167
#, kde-format
msgid "Recording Devices"
msgstr "Dispositivi di registrazione"

#: kcm/ui/main.qml:191
#, kde-format
msgid "Inactive Cards"
msgstr "Schede inattive"

#: kcm/ui/main.qml:222
#, kde-format
msgid "Playback Streams"
msgstr "Riproduzione flussi"

#: kcm/ui/main.qml:271
#, kde-format
msgid "Recording Streams"
msgstr "Registrazione flussi"

#: kcm/ui/main.qml:305
#, kde-format
msgctxt ""
"%1 is an error string produced by an external component, and probably "
"untranslated"
msgid ""
"Error trying to play a test sound. \n"
"The system said: \"%1\""
msgstr ""
"Errore durante il tentativo di riprodurre un suono di prova.\n"
"Il sistema ha restituito: «%1»"

#: kcm/ui/main.qml:402
#, kde-format
msgid "Front Left"
msgstr "Anteriore sinistro"

#: kcm/ui/main.qml:403
#, kde-format
msgid "Front Center"
msgstr "Anteriore centrale"

#: kcm/ui/main.qml:404
#, kde-format
msgid "Front Right"
msgstr "Anteriore destro"

#: kcm/ui/main.qml:405
#, kde-format
msgid "Side Left"
msgstr "Laterale sinistro"

#: kcm/ui/main.qml:406
#, kde-format
msgid "Side Right"
msgstr "Laterale destro"

#: kcm/ui/main.qml:407
#, kde-format
msgid "Rear Left"
msgstr "Posteriore sinistro"

#: kcm/ui/main.qml:408
#, kde-format
msgid "Subwoofer"
msgstr "Subwoofer"

#: kcm/ui/main.qml:409
#, kde-format
msgid "Rear Right"
msgstr "Posteriore destro"

#: kcm/ui/main.qml:410
#, kde-format
msgid "Mono"
msgstr "Mono"

#: kcm/ui/main.qml:464
#, kde-format
msgid "Click on any speaker to test sound"
msgstr "Fare clic su qualsiasi altoparlante per provare il suono"

#: kcm/ui/MuteButton.qml:25
#, kde-format
msgctxt "Unmute audio stream"
msgid "Unmute %1"
msgstr "Rimuovi silenzio per %1"

#: kcm/ui/MuteButton.qml:25
#, kde-format
msgctxt "Mute audio stream"
msgid "Mute %1"
msgstr "Silenzia %1"

#: kcm/ui/StreamListItem.qml:57
#, kde-format
msgid "Notification Sounds"
msgstr "Suoni delle notifiche"

#: kcm/ui/StreamListItem.qml:63
#, kde-format
msgctxt "label of stream items"
msgid "%1: %2"
msgstr "%1: %2"

#: kcm/ui/VolumeControlsConfig.qml:15
#, kde-format
msgid "Volume Controls"
msgstr "Controlli del volume"

#: kcm/ui/VolumeControlsConfig.qml:31
#, kde-format
msgid "Raise maximum volume"
msgstr "Alza il volume al massimo"

#: kcm/ui/VolumeControlsConfig.qml:39
#, kde-format
msgid "Volume change step:"
msgstr "Passo di modifica del volume:"

#: kcm/ui/VolumeControlsConfig.qml:63
#, kde-format
msgid "Play audio feedback for changes to:"
msgstr "Riproduci un riscontro sonoro per le modifiche a:"

#: kcm/ui/VolumeControlsConfig.qml:64 kcm/ui/VolumeControlsConfig.qml:73
#, kde-format
msgid "Audio volume"
msgstr "Volume audio"

#: kcm/ui/VolumeControlsConfig.qml:72
#, kde-format
msgid "Show visual feedback for changes to:"
msgstr "Mostra un riscontro visivo per le modifiche a:"

#: kcm/ui/VolumeControlsConfig.qml:80
#, kde-format
msgid "Microphone sensitivity"
msgstr "Sensibilità del microfono"

#: kcm/ui/VolumeControlsConfig.qml:87
#, kde-format
msgid "Mute state"
msgstr "Stato silenziato"

#: kcm/ui/VolumeControlsConfig.qml:94
#, kde-format
msgid "Default output device"
msgstr "Dispositivo di uscita predefinito"

#: kcm/ui/VolumeSlider.qml:68
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: kcm/ui/VolumeSlider.qml:88
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: kded/audioshortcutsservice.cpp:58
#, kde-format
msgid "Increase Volume"
msgstr "Aumenta volume"

#: kded/audioshortcutsservice.cpp:73
#, kde-format
msgid "Decrease Volume"
msgstr "Riduci volume"

#: kded/audioshortcutsservice.cpp:88
#, kde-format
msgid "Increase Volume by 1%"
msgstr "Aumenta il volume del 1%"

#: kded/audioshortcutsservice.cpp:103
#, kde-format
msgid "Decrease Volume by 1%"
msgstr "Riduci il volume del 1%"

#: kded/audioshortcutsservice.cpp:118
#, kde-format
msgid "Increase Microphone Volume"
msgstr "Aumenta volume del microfono"

#: kded/audioshortcutsservice.cpp:132
#, kde-format
msgid "Decrease Microphone Volume"
msgstr "Riduci volume del microfono"

#: kded/audioshortcutsservice.cpp:146 qml/microphoneindicator.cpp:106
#, kde-format
msgid "Mute"
msgstr "Silenzia"

#: kded/audioshortcutsservice.cpp:159
#, kde-format
msgid "Mute Microphone"
msgstr "Silenzia microfono"

#: kded/audioshortcutsservice.cpp:173
#, kde-format
msgid "Audio Volume"
msgstr "Volume audio"

#: kded/audioshortcutsservice.cpp:191
#, kde-format
msgid "No such device"
msgstr "Nessun dispositivo"

#: kded/audioshortcutsservice.cpp:207
#, kde-format
msgid "Device name not found"
msgstr "Nome del dispositivo non trovato"

#: kded/audioshortcutsservice.cpp:239
#, kde-format
msgid "No output device"
msgstr "Nessun dispositivo di uscita"

#: kded/audioshortcutsservice.cpp:253
#, kde-format
msgctxt "Device name (Battery percent)"
msgid "%1 (%2% Battery)"
msgstr "%1 (%2% batteria)"

#: qml/listitemmenu.cpp:319
#, kde-format
msgid "Play all audio via this device"
msgstr "Riproduci tutto l'audio tramite questo dispositivo"

#: qml/listitemmenu.cpp:324
#, kde-format
msgid "Record all audio via this device"
msgstr "Registra tutto l'audio tramite questo dispositivo"

#: qml/listitemmenu.cpp:352
#, kde-format
msgctxt ""
"Heading for a list of ports of a device (for example built-in laptop "
"speakers or a plug for headphones)"
msgid "Ports"
msgstr "Porte"

#: qml/listitemmenu.cpp:422
#, kde-format
msgctxt ""
"Heading for a list of device profiles (5.1 surround sound, stereo, speakers "
"only, ...)"
msgid "Profiles"
msgstr "Profili"

#: qml/listitemmenu.cpp:456
#, kde-format
msgctxt ""
"Heading for a list of possible output devices (speakers, headphones, ...) to "
"choose"
msgid "Play audio using"
msgstr "Riproduci audio utilizzando"

#: qml/listitemmenu.cpp:458
#, kde-format
msgctxt ""
"Heading for a list of possible input devices (built-in microphone, "
"headset, ...) to choose"
msgid "Record audio using"
msgstr "Registra audio utilizzando"

#: qml/microphoneindicator.cpp:136 qml/microphoneindicator.cpp:138
#, kde-format
msgid "Microphone"
msgstr "Microfono"

#: qml/microphoneindicator.cpp:138
#, kde-format
msgid "Microphone Muted"
msgstr "Microfono silenziato"

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "list separator"
msgid ", "
msgstr ", "

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "List of apps is using mic"
msgid "%1 are using the microphone"
msgstr "%1 stanno utilizzando il microfono"

#: qml/microphoneindicator.cpp:310
#, kde-format
msgctxt "App %1 is using mic with name %2"
msgid "%1 is using the microphone (%2)"
msgstr "%1 sta utilizzando il microfono (%2)"

#: qml/microphoneindicator.cpp:317
#, kde-format
msgctxt "App is using mic"
msgid "%1 is using the microphone"
msgstr "%1 sta utilizzando il microfono"

#~ msgctxt "Name shown in debug pulseaudio tools"
#~ msgid "Plasma PA"
#~ msgstr "Plasma PA"

#~ msgid "This module allows configuring the Pulseaudio sound subsystem."
#~ msgstr ""
#~ "Questo modulo consente di configurare il sottosistema sonoro Pulseaudio."

#~ msgid "100%"
#~ msgstr "100%"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Vincenzo Reale"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "smart2128vr@gmail.com "

#~ msgctxt "@title"
#~ msgid "Audio"
#~ msgstr "Audio"

#~ msgctxt "@info:credit"
#~ msgid "Copyright 2015 Harald Sitter"
#~ msgstr "Copyright 2015 Harald Sitter"

#~ msgctxt "@info:credit"
#~ msgid "Harald Sitter"
#~ msgstr "Harald Sitter"

#~ msgctxt "@info:credit"
#~ msgid "Author"
#~ msgstr "Autore"

#~ msgid "Configure"
#~ msgstr "Configura"

#~ msgid "Device Profiles"
#~ msgstr "Profili di dispositivo"

#~ msgid "Advanced Output Configuration"
#~ msgstr "Configurazione di uscita avanzata"

#~ msgid "Speaker Placement and Testing"
#~ msgstr "Posizionamento degli altoparlanti e test"

#~ msgctxt "@label"
#~ msgid "Output:"
#~ msgstr "Uscita:"

#~ msgctxt "Port is unavailable"
#~ msgid " (unavailable)"
#~ msgstr " (non disponibile)"

#~ msgctxt "Port is unplugged"
#~ msgid " (unplugged)"
#~ msgstr " (non collegata)"

#~ msgid "Configure..."
#~ msgstr "Configura..."

#~ msgctxt "@label"
#~ msgid "No Device Profiles Available"
#~ msgstr "Nessun profilo di dispositivo disponibile"

#~ msgctxt "@label"
#~ msgid "No Playback Devices Available"
#~ msgstr "Nessun dispositivo di riproduzione disponibile"

#~ msgctxt "@label"
#~ msgid "No Recording Devices Available"
#~ msgstr "Nessun dispositivo di registrazione disponibile"

#~ msgctxt "@label"
#~ msgid "No Applications Playing Audio"
#~ msgstr "Nessuna applicazione che riproduce audio"

#~ msgctxt "@label"
#~ msgid "No Applications Recording Audio"
#~ msgstr "Nessuna applicazione che registra audio"

#~ msgctxt "@title:tab"
#~ msgid "Devices"
#~ msgstr "Dispositivi"

#~ msgctxt "@title:tab"
#~ msgid "Applications"
#~ msgstr "Applicazioni"

#~ msgctxt "@title:tab"
#~ msgid "Advanced"
#~ msgstr "Avanzate"

#~ msgid "Outputs"
#~ msgstr "Uscite"

#~ msgctxt "@label"
#~ msgid "No Output Devices Available"
#~ msgstr "Nessun dispositivo di uscita disponibile"

#~ msgid "Inputs"
#~ msgstr "Ingressi"

#~ msgctxt "@title"
#~ msgid "Configure the Audio Volume"
#~ msgstr "Configura il volume audio"

#~ msgid "Capture"
#~ msgstr "Cattura"

#~ msgctxt "@label"
#~ msgid "No Additional Configuration Available"
#~ msgstr "Nessuna configurazione aggiuntiva disponibile"
